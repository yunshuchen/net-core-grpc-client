﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Google.Protobuf;
using Grpc.Core;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace GrpcDemo
{
    public class Program
    {
        public static void Main(string[] args)
        {
           /* var options = new List<ChannelOption> {
                new ChannelOption(ChannelOptions.MaxReceiveMessageLength,int.MaxValue),
                new ChannelOption(ChannelOptions.MaxSendMessageLength,int.MaxValue)
            };
            Channel channel = new Channel("127.0.0.1:8088", ChannelCredentials.Insecure, options);
            var client = new DocsFileConvert.DocsFileConvertClient(channel);
            GrpcDemo.RequestConvertFile request = new RequestConvertFile();
            request.Index = 1;
            request.InputFilePath = @"C:\Users\Administrator\Desktop\jinbi.mpp";
            request.OutputFilePath = @"C:\output\jinbi_nowatermark.html";
            request.WaterMarkFilePath =@"D:\Works\zhhz_docs\store\watermark.pdf";
            request.WaterMarkedPath = @"C:\output\jinbi.html";
            request.WaterMarkText = "";
            var res = client.Convert(request);*/

          /*  var client = new DocsStreamConvert.DocsStreamConvertClient(channel);
            RequestInputFileStream request = new RequestInputFileStream();
            request.InputStream = ByteString.FromStream(ExtImage.FileToStream(@"D:\\123.dwg"));
            request.Index = 1;
            request.InFileName = "123456.dwg";

            request.OutFileName = "dwg.pdf";

            //建议pdf才添加水印添加文件水印
            request.WaterMarkStream = ByteString.Empty;
            request.WaterMarkFileName = "";
            //添加文字水印
            request.WaterMarkText = "";
            var res = client.Convert(request);
            if (res.Status == 1)
            {
                var res_bytes = res.OutputStream.ToByteArray();
                ExtImage.BytesToFile(res_bytes, @"C:\output\dwg.pdf");
            }*/
            CreateHostBuilder(args).Build().Run();

    
        }

        // Additional configuration is required to successfully run gRPC on macOS.
        // For instructions on how to configure Kestrel and gRPC clients on macOS, visit https://go.microsoft.com/fwlink/?linkid=2099682
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
